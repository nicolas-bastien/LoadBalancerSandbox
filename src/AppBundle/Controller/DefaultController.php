<?php

namespace AppBundle\Controller;

use NBN\LoadBalancer\LoadBalancer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Nicolas Bastien <nbastien@prestaconcept.net>
 */
class DefaultController extends Controller
{
    /**
     * @param  Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var LoadBalancer $loadbalancer */
        $loadbalancer = $this->get('nbn_loadbalancer.loadbalancer');

        $response = $loadbalancer->handleRequest($request);

        return $this->render('default/index.html.twig', [
            'status'    => $response->getStatusCode(),
            'content'   => $response->getContent(),
            'handledBy' => $response->headers->get('Handled-By')
        ]);
    }
}
