ENV?=dev

# ====================
# Qualimetry rules

## Run all qualimetry tasks
qualimetry: lint checkstyle

## Qualimetry : lint
lint:
	find -L src -name '*.php' -print0 | xargs -0 -n 1 -P 4 php -l

## Qualimetry : checkstyle
cs: checkstyle
checkstyle:
	vendor/bin/phpcs --extensions=php -n --standard=PSR2 --report=full src

# ======================
# Docker

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

restart:
	docker-compose restart

ps:
	docker-compose ps

bash:
	docker exec -it --user=www-data loadbalancer-phpapache bash

# ====================
# Doctrine ORM rules

## Database install
orm.install:
	echo "Database not activated"
	#php bin/console doctrine:database:create --env=$(ENV)
	#php bin/console doctrine:schema:create --env=$(ENV)
	#php bin/console doctrine:fixtures:load --no-interaction --env=$(ENV)
	#php bin/console doctrine:migration:version --add --all --no-interaction --env=$(ENV)

## Database uninstall
orm.uninstall:
	echo "Database not activated"
	#php bin/console doctrine:database:drop --force --env=$(ENV)

## Database update
orm.update:
	echo "DoctrineMigrationsBundle not activated"
	#php bin/console doctrine:migrations:migrate --no-interaction --env=$(ENV)

## Database rebuild
orm.rebuild: orm.uninstall orm.install

load.fake:
	echo "Database not activated"
	#php bin/console doctrine:fixtures:load --no-interaction --env=$(ENV) --append --fixtures=src/AppBundle/DataFakes/ORM/

# ====================
# Install rules

dependencies:
	composer install
	npm install
	./node_modules/.bin/bower install
	npm run build

install: orm.install cc

dev.install: dependencies install load.fake

test.install: override ENV=test
test.install: dependencies install

# ====================
# Uninstall rules

uninstall: orm.uninstall

test.uninstall: override ENV=test
test.uninstall: uninstall

## Cache clear
cc:
	php bin/console cache:clear --env=$(ENV)

ccp: override ENV=prod
ccp:  cc
