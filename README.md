LoadBalancerSandbox
===================

## Overview

**Important** : This is a training projet, it's only purpose is to demonstrated how to develop integrate an open source component in a Symfony2 application.

## Description

This project has been made to demonstrates :

- how to build a project with gitlab ci
- how to overide an open source bundle service
- how to build assets with npm
- how to provide a development environnement with docker


## Installation

This projects use *make* to ease common task, so this should be available on your system.

### Get the code

    git clone git@gitlab.com:nicolas-bastien/LoadBalancerSandbox.git
    cd LoadBalancerSandbox
    
### Run docker environment
    
    make build
    make up
    
### Install the application

    make bash
    make dev.install

You should be able the get the homepage below by openning this url in your browser : http://loadbalancer.dev/app_dev.php

![Homepage](https://gitlab.com/nicolas-bastien/LoadBalancerSandbox/raw/master/doc/images/homepage.png)

### Common problems

If your apache container does not start, check if you have a local apache running, stop it and make up again.

---

*Developed by [Nicolas Bastien](https://github.com/nicolas-bastien)*

Released under the MIT License
